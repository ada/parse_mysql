# Parse Mysql db

## Scope
Get snapshot of database including creation of:

  - tables
  - triggers
  - procedures
  - functions
 
Save info into files with bellow file structures:

  + ${configurable_name_of_folder, f.e. company-production:people}
    + ${folder_for_table}
      - ${table_name}.sql # data about how is created table
      - ${table_name}.meta # metatada related to table
      + triggers # folder for trigers attached to this table
         - ${trigger_name}.sql
         - ${trigger_name}.meta
    + ${folder_procedures} # default is __PROCEDURES__
         - ${procedure_name}.sql
         - ${procedure_name}.meta
    + ${folder_functions} # default is __FUNCTIONS__
         - ${function_name}.sql
         - ${function_name}.meta

## Requirements
  - packages.txt
  - requirements.txt
 
## Usage
  - edit parse_mysql_config.py # set up configuration in main functions  
  - ./parse_mysql_config.py # run script
  - save oput in your repository/storage for your purposes