#!/usr/bin/python
import MySQLdb
import re
import os

def get_mysql_cursor():
    db_mysql = MySQLdb.connect(
        host=MYSQL_HOST,
        port=MYSQL_PORT,
        user=MYSQL_USER,
        passwd=MYSQL_PASS,
        db=MYSQL_DB
    )
    return db_mysql.cursor()


def parse_trigger(cursor, table_name):
    cursor.execute("SHOW TRIGGERS LIKE '%s'" % table_name)
    res = cursor.fetchone()

    if res:
        trigger_name, event, table_name, statement, timing, created, sql_mode, definer, \
            character_set_client, collation_connection, database_collation = res

        print "\t trigger: %s" % trigger_name

        try:
            os.mkdir(os.path.join(TABLES_FOLDER, table_name, "triggers"))
        except OSError, e:
            pass

        open(os.path.join(TABLES_FOLDER, table_name, "triggers", trigger_name + ".meta"), "w").write("""\
trigger_name: %s,
event: %s,
table_name: %s,
statement: %s,
timing: %s,
created: %s,
sql_mode: %s,
definer: %s,
character_set_client: %s,
collation_connection: %s,
database_collation: %s,
""" %
            (
                trigger_name,
                event,
                table_name,
                "see file",
                timing,
                created,
                sql_mode,
                definer,
                character_set_client,
                collation_connection,
                database_collation
            ))

        open(os.path.join(TABLES_FOLDER, table_name, "triggers", trigger_name + ".sql"), "w").write(statement)


def parse_table(cursor, table_name):
    cursor.execute("SHOW CREATE TABLE %s" % table_name)
    res = cursor.fetchone()
    table_name, create_table = res[0], res[1]

    if REMOVE_AUTOINCREMENT:
        create_table = re.sub("AUTO_INCREMENT=\d+ ", "", create_table)

    if REMOVE_COMMENT:
        create_table = re.sub("COMMENT='.*?'", "", create_table)

    print "table: %s" % table_name

    try:
        os.mkdir(os.path.join(TABLES_FOLDER, table_name))
    except OSError, e:
        pass

    #print res
    # if len(res) > 2:
    #     print "--", res
    open(os.path.join(TABLES_FOLDER, table_name, table_name + ".sql"), "w").write(create_table)

    # TRIGGERS
    parse_trigger(cursor, table_name)


def parse_stored_routines(cursor, routine_name):
    cursor.execute("SHOW %s STATUS WHERE Db = '%s'" % (routine_name, MYSQL_DB))
# *************************** 92. row ***************************
#                   Db: carloc3
#                 Name: _TEST_TMS_Secs_to_dhhmmss
#                 Type: PROCEDURE
#              Definer: some_user@%
#             Modified: 2013-01-31 11:13:28
#              Created: 2013-01-31 11:13:28
#        Security_type: DEFINER
#              Comment: $Id: 20120521092322_procs.sql 4695 2012-05-21 12:35:14Z some_user $
# character_set_client: utf8
# collation_connection: utf8_general_ci
#   Database Collation: utf8_general_ci

    for db, name, type_, definer, modified, created, security_type, comment, character_set_client, \
        collation_connection, database_collation in cursor.fetchall():

        try:
            os.mkdir(os.path.join(TABLES_FOLDER, ROUTINES[routine_name]))
        except OSError, e:
            pass
        # SHOW CREATE FUNCTION FxCarLoc4GetZoneNameReduced;
        cursor.execute("SHOW CREATE %s %s" % (routine_name, name))
# *************************** 1. row ***************************
#            Procedure: _TEST_Run
#             sql_mode: NO_AUTO_VALUE_ON_ZERO
#     Create Procedure: CREATE DEFINER=`some_user`@`%` PROCEDURE `_TEST_Run`()
#     COMMENT '$Id: 20120521092322_procs.sql 4695 2012-05-21 12:35:14Z some_user $'
# BEGIN DECLARE ident VARCHAR(20) DEFAULT '_TEST_Run'; CALL debug.debug_on(ident); CALL _TEST_TMS_Secs_to_dhhmmss(0, 0, 0, 12, '0 00:00:12'); CALL _TEST_TMS_Secs_to_dhhmmss(0, 0, 0, 59, '0 00:00:59'); CALL _TEST_TMS_Secs_to_dhhmmss(0, 0, 0, 60, '0 00:01:00'); CALL _TEST_TMS_Secs_to_dhhmmss(0, 0, 57, 0, '0 00:57:01'); CALL _TEST_TMS_Secs_to_dhhmmss(0, 0, 59, 60, '0 01:00:00'); CALL _TEST_TMS_Secs_to_dhhmmss(0, 22, 11, 47, '0 22:11:47'); CALL _TEST_TMS_Secs_to_dhhmmss(44, 23, 59, 59, '44 23:59:59'); CALL _TEST_TMS_Secs_to_dhhmmss(0, 0, 0, -59, '-0 00:00:59'); CALL _TEST_TMS_Secs_to_dhhmmss(-44, -23, -59, -59, '-44 23:59:59'); CALL debug.debug_off(ident); END
# character_set_client: utf8
# collation_connection: utf8_general_ci
#   Database Collation: utf8_general_ci
        name, sql_mode, create_procedure, character_set_client, collation_connection, database_collation = cursor.fetchone()
        open(os.path.join(TABLES_FOLDER, ROUTINES[routine_name], name + ".sql"), "w").write(create_procedure)
        open(os.path.join(TABLES_FOLDER, ROUTINES[routine_name], name + ".meta"), "w").write("""\
name: %s,
sql_mode: %s,
character_set_client: %s,
collation_connection: %s,
database_collation: %s,
type: %s,
definer: %s,
modified: %s,
created: %s,
security_type: %s,
comment: %s\
""" % (
            name,
            sql_mode,
            character_set_client,
            collation_connection,
            database_collation,
            type_,
            definer,
            modified,
            created,
            security_type,
            comment
        ))
        print "%s: %s" % (routine_name, name)


def main():
    cursor_mysql = get_mysql_cursor()

    cursor_mysql.execute("SHOW TABLES")

    try:
        os.mkdir(TABLES_FOLDER)
    except OSError, e:
        pass

    for table_name, in cursor_mysql.fetchall():
        parse_table(cursor_mysql, table_name)

    parse_stored_routines(cursor_mysql, "PROCEDURE")
    parse_stored_routines(cursor_mysql, "FUNCTION")


if __name__ == "__main__":
    ROUTINES = {
        "PROCEDURE": "__PROCEDURES__", # create folder to store procedures
        "FUNCTION": "__FUNCTIONS__" # create folder to store functions
    }
    
    REMOVE_AUTOINCREMENT = True # cut AUTOINCREMENT info from create table script
    REMOVE_COMMENT = True # cut COMMENT info from create table script
    MYSQL_HOST = "127.0.0.1"
    MYSQL_PORT = 3306
    MYSQL_USER = "root"
    MYSQL_PASS = ""
    MYSQL_DB = "people"
    TABLES_FOLDER="company-prod:people"

    main()
